import React, {memo} from 'react';
import './App.scss';
import TreeView from "./modules/tree/views/TreeView";

interface Props {
}

function App(): React.ReactElement<Props> {
  return (
    <div className="App">
      <TreeView />
    </div>
  );
}

export default memo(App);
