export interface TreeData {
  ID: number;
  VALUE: string;
  PARENTID: number | null;
  CANCELED: boolean;
}

export interface Tree {
  key: number;
  value: string;
  canceled: boolean;
  children: Tree[];
  parentId: number | null;
}

export interface SerializedDBRecord {
  key: number;
  value: string;
  canceled: boolean;
  parentId: number | null;
}
