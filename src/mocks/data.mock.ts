import {SerializedDBRecord, TreeData} from "./types/treeData.interface";
import {dataToTree} from "../modules/tree/services/dbApiService";
import localForage from "localforage";

const dataMock = [{ ID: 1, VALUE: "First node value", PARENTID: 0, CANCELED: false },
    { ID: 2, VALUE: "Some val 1", PARENTID: 1, CANCELED: false },
    { ID: 3, VALUE: "Some val 2", PARENTID: 2, CANCELED: false },
    { ID: 4, VALUE: "Some val 3", PARENTID: 3, CANCELED: false },
    { ID: 5, VALUE: "Some val 4", PARENTID: 4, CANCELED: false },
    { ID: 6, VALUE: "Some val 5", PARENTID: 4, CANCELED: false },
    { ID: 7, VALUE: "Some val 6", PARENTID: 4, CANCELED: false },
    { ID: 8, VALUE: "Second node value", PARENTID: 0, CANCELED: false },
    { ID: 9, VALUE: "Some val 8", PARENTID: 8, CANCELED: false },
    { ID: 10, VALUE: "Some val 9", PARENTID: 9, CANCELED: false },
    { ID: 11, VALUE: "Some val 10", PARENTID: 10, CANCELED: false },
    { ID: 12, VALUE: "Some val 11", PARENTID: 11, CANCELED: false },
    { ID: 13, VALUE: "Some val 12", PARENTID: 11, CANCELED: false }
];

/*
 * Need to be serialized because we don't know what kind of data receive from DB
 */
export const serializedDB = ((): SerializedDBRecord[] => {
  const sDB: SerializedDBRecord[] = [];
  dataMock.forEach((item: TreeData) => {
    sDB.push({key: item.ID, value: item.VALUE, canceled: item.CANCELED, parentId: item.PARENTID});
  });
  return sDB;
})();

export const dbInstance = (() => {
  return localForage.createInstance({
    name: "dataStore"
  });
})();

export const tree = dataToTree(serializedDB);