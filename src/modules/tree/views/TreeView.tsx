import React, {memo} from "react";
import Header from "../components/organisms/dbTree/Header";
import LayoutCentered from "layouts/LayoutCentered";
import LayoutColumns from "layouts/LayoutColumns";
import CachedTree from "../components/organisms/cacheTree/CachedTree";
import DBTree from "../components/organisms/dbTree/DBTree";
import DBTreeJsonView from "../components/organisms/dbTree/DBTreeJsonView";
import {TreeContext} from "../contexts/TreeContext";
import {useTreeContext} from "../hooks/useTreeContext";

import '../styles/common.scss';

interface Props {
}

const TreeView: React.FC<Props> = (): React.ReactElement<Props> | null => {
  const treeContext = useTreeContext();

  return (
    <TreeContext.Provider value={treeContext}>
      <LayoutCentered>
        <Header />
        <LayoutColumns>
          <DBTreeJsonView />
          <DBTree />
          <CachedTree />
        </LayoutColumns>
      </LayoutCentered>
    </TreeContext.Provider>
  );
};

export default memo(TreeView);