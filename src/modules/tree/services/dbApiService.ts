import { serializedDB } from "mocks/data.mock";
import {SerializedDBRecord, Tree} from "mocks/types/treeData.interface";
import {cloneDeep} from "lodash";
import localForage from "localforage";

export const fetchItem = async (key: number): Promise<SerializedDBRecord>=> {
  const dbClone = cloneDeep(serializedDB);
  const itemToFetch: SerializedDBRecord = dbClone[dbClone.findIndex(item => item.key === key)];
  //Save in cache (IndexedDB)
  try {
    await localForage.setItem(String(key), itemToFetch);
    return itemToFetch;
  } catch (err) {
    // Here should be a custom Logger that logs to Sentry
    console.log(err);
    return null;
  }
}

export function flattenTree(data: Tree[]): Tree[] {
  return data.reduce((arr, {key, value, parentId, canceled, children = []}) =>
    arr.concat([{key, value, parentId, canceled}], dataToTree(children)), [])
}

export function dataToTree(dataset: SerializedDBRecord[]): Tree[] {
  if(!dataset || dataset.length === 0) return;
  const hashTable = Object.create(null);
  dataset.forEach(data => hashTable[data.key] = {...data, children: []});
  const dataTree = [] as Tree[];
  dataset.forEach(data => {
    if(data.parentId) {
      //TODO: !!!Refactor - Restore hack
      if(!hashTable[data.parentId]?.canceled || (hashTable[data.parentId]?.canceled && data.canceled)) hashTable[data.parentId]?.children.push(hashTable[data.key])
      if(!hashTable[data.parentId] || (hashTable[data.parentId].canceled && !data.canceled)) dataTree.push(hashTable[data.key]);
    }
    else dataTree.push(hashTable[data.key])
  });
  return dataTree.sort((a, b) => a.key - b.key);
}
