import {SerializedDBRecord} from "mocks/types/treeData.interface";
import localForage from "localforage";
import {dbInstance} from "mocks/data.mock";

export const fetchItemsFromCache = async (): Promise<SerializedDBRecord[]> => {
  //Fetch from cache (IndexedDB)
  const cacheItems = [] as SerializedDBRecord[];
  try {
    await localForage.iterate((value: SerializedDBRecord, key: string) => {
      cacheItems.push(value);
    });
    return cacheItems;
  } catch (err) {
    // Here should be a custom Logger that logs to Sentry
    console.log(err);
    return [] as SerializedDBRecord[];
  }
}

export const checkModifiedDB = async (): Promise<SerializedDBRecord[]> => {
  return await dbInstance.getItem("db");
}

export const updateItemsInCache = async (nodes: SerializedDBRecord[]) => {
  await localForage.iterate((node: SerializedDBRecord, key: string) => {
    nodes.forEach(n => {
      if(n.key === node.key)
        node.canceled = n.canceled
        localForage.setItem(key, node);
    })
  });
}

export const updateCacheItem = async (key: number, value: string | boolean, prop: string) => {
  const item: SerializedDBRecord = await localForage.getItem(String(key));
  item[prop] = value;
  await localForage.setItem(String(key), item);
}

export function saveChangesInDB(db: SerializedDBRecord[], cache: SerializedDBRecord[]): SerializedDBRecord[] {
  const updatedDB = db?.map(item => {
    const cacheObj = cache.find(o => o.key === item.key);
    if(cacheObj) {
      item.value = cacheObj.value;
      item.canceled = cacheObj.canceled;
    }
    return {...item};
  });
  dbInstance.setItem("db", updatedDB);
  return updatedDB;
}

export function changeTreeState(arr: SerializedDBRecord[], key: number, value: boolean): SerializedDBRecord[] {
  const i = arr.findIndex(item => item.key === key);
  arr[i].canceled = value;

  return (function changeChildState(arr: SerializedDBRecord[], key: number, value: boolean): SerializedDBRecord[] {
    const childrenToDelete = [...arr].filter(item => item.parentId === key);
    childrenToDelete.forEach((item: SerializedDBRecord) => {
      item.canceled = value;
      changeChildState(arr, item.key, value);
    })
    return arr;
  })(arr, key, value);
}
