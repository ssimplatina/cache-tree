import React from "react";
import {TreeContextProps} from "../types/dbTypes";

const TreeContext: React.Context<Partial<TreeContextProps>> = React.createContext<Partial<TreeContextProps>>(
  {
    selectedKey: 0,
    cacheItems: [],
    cacheItem: null,
    tree: [],
    db: []
  }
);
export { TreeContext };