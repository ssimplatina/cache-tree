import {useContext, useEffect, useMemo, useState} from "react";
import {cloneDeep} from "lodash";
import {TreeContext} from "../contexts/TreeContext";
import { serializedDB } from "mocks/data.mock";
import {SerializedDBRecord, Tree} from "../../../mocks/types/treeData.interface";
import {fetchItemsFromCache, checkModifiedDB} from "../services/cacheService";
import {dataToTree} from "../services/dbApiService";
import {TreeContextProps} from "../types/dbTypes";


export function useTreeContext(): Partial<TreeContextProps> {
  const [db, setDB] = useState<SerializedDBRecord[]>(cloneDeep(serializedDB));
  const [selectedKey, setSelectedKey] = useState<number | null>(null);
  const [cacheItems, setCacheItems] = useState<SerializedDBRecord[]>([]);
  const [cacheItem, setCacheItem] = useState<SerializedDBRecord | null>(null);

  useEffect(() => {
    (async () => {
      const cacheData = await fetchItemsFromCache()
      setCacheItems(cacheData);
      //Check for saved DB
      const data = await checkModifiedDB();
      data ? setDB(data) : setDB(cloneDeep(serializedDB));
    })();
  }, []);

  const tree: Tree[] = useMemo(
    () => dataToTree(cacheItems),
    [cacheItems]
  );

  return useMemo(() => ({selectedKey, setSelectedKey, cacheItems, setCacheItems, tree, cacheItem, setCacheItem, db, setDB}), [selectedKey, setSelectedKey, cacheItems, setCacheItems, tree, cacheItem, setCacheItem, db, setDB]);
}

export function useTree(): Partial<TreeContextProps> {
  return useContext(TreeContext);
}