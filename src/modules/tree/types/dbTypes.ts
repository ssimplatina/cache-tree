import { SerializedDBRecord, Tree } from "../../../mocks/types/treeData.interface";

export interface TreeContextProps {
  db: SerializedDBRecord[];
  setDB: (db: SerializedDBRecord[]) => void;
  selectedKey: number;
  setSelectedKey: (key: number) => void;
  cacheItems: SerializedDBRecord[];
  setCacheItems: (items: SerializedDBRecord[]) => void;
  cacheItem: SerializedDBRecord | null;
  setCacheItem: (item: SerializedDBRecord) => void;
  tree: Tree[];
  setCacheTree: (items: Tree[]) => void;
}