import React from "react";
import { Button } from 'antd';

interface Props {
  callback?: Function;
  label?: string;
  shape?: "circle" | "round";
  size?: "large" | "small";
  icon?: React.ReactNode
}

export default function TButton({callback, label, shape, size, icon}: Props): React.ReactElement<Props> | null {
  return (
    <div className='tbutton'>
      <Button onClick={() => callback()} shape={shape} size={size} icon={icon}>{label}</Button>
    </div>
  );
};