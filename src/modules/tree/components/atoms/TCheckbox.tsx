import React, {memo, useCallback} from "react";
import { Checkbox } from 'antd';
import { SerializedDBRecord } from "../../../../mocks/types/treeData.interface";

import '../styles/TCheckbox.scss';

interface Props {
  selected?: boolean;
  label: string;
  onChange: (key: number) => void;
  nodeKey: number;
  node?: SerializedDBRecord;
}

const TCheckbox: React.FC<Props> = ({ selected, label, onChange, node, nodeKey }: Props): React.ReactElement<Props> => {

  const isChecked = node?.key === nodeKey;

  const handleChange = useCallback((value: boolean, key: number) => {
    if (value)
      onChange(key);
  }, [onChange]);

  return (
    <Checkbox disabled={node.canceled} checked={isChecked} onChange={e => handleChange(e.target.checked, node.key)}>{label}</Checkbox>
  )
}

export default memo(TCheckbox);