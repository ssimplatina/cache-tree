import React from "react";
import TButton from "../atoms/TButton";

import '../styles/common.scss';

interface Props {
  customClass?: string;
  callback?: Function;
  label?: string;
  shape?: "circle" | "round";
  size?: "large" | "small";
  icon?: React.ReactNode
}

export default function TStyledButton({customClass, callback, label, size, icon, shape}: Props): React.ReactElement<Props> | null {
  return (
    <div className={customClass}>
      <TButton size={size} callback={callback} label={label} icon={icon} shape={shape} />
    </div>
  );
};