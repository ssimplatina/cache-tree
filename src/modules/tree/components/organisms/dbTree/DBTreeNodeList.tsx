import React, {useCallback, memo} from "react";
import { SerializedDBRecord } from "../../../../../mocks/types/treeData.interface";
import TCheckbox from "../../atoms/TCheckbox";
import {useTree} from "../../../hooks/useTreeContext";

interface Props {
  nodes: SerializedDBRecord[];
}
/* @ts-ignore */
const DBTreeNodeList: React.FC<Props> = ({ nodes }: Props): React.ReactElement<Props> => {

  const {selectedKey, setSelectedKey} = useTree();

  const handleCheckboxClicked = useCallback((selectedNodeId: number) => {
    setSelectedKey(selectedNodeId);
  }, [setSelectedKey]);

  /* @ts-ignore */
  return (
    <div>
      {nodes?.map((node: SerializedDBRecord) => (
        <ul key={node.key}>
          <TCheckbox
            label={node.value}
            onChange={() => {handleCheckboxClicked(node.key)}}
            nodeKey={selectedKey}
            node={node}
          />
        </ul>
      ))}
    </div>
  )
}

export default memo(DBTreeNodeList);