import React, {useCallback} from "react";
import {cloneDeep} from "lodash";
import TButton from "../../atoms/TButton";
import {useTree} from "../../../hooks/useTreeContext";
import { serializedDB, dbInstance } from "mocks/data.mock";
import localForage from "localforage";

import '../../styles/common.scss';

interface Props {
}

const Header: React.FC<Props> = (): React.ReactElement<Props> | null => {
  const {setCacheItems, setDB, setSelectedKey} = useTree();

  const resetApp = useCallback(async () => {
    //reset store to initial value
    //clean cache
    //restore db
    await localForage.clear();
    await dbInstance.clear();
    setCacheItems([]);
    setDB(cloneDeep(serializedDB));
    setSelectedKey(null);
  }, [setCacheItems, setDB, setSelectedKey]);

  return (
    <div className='header-wrapper'>
      <TButton callback={resetApp} label={'Reset app'} />
    </div>
  );
};

export default Header;