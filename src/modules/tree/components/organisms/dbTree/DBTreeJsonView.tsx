import React, {memo} from "react";
import { tree } from "mocks/data.mock";
import JSONTree from 'react-json-tree';

import '../../styles/DBTree.scss';

/*
 * !!!ONLY TO VIEW DATA STRUCTURE
 */
interface Props {
}

const DBTreeJsonView: React.FC<Props> = (): React.ReactElement<Props> | null => {
  const getItemString = (type: string): React.ReactElement => (
    <span>
      {type}
    </span>
  );

  return (
    <div className='db-tree-viewer'>
      <h2>DBTreeJson</h2>
      <h5>Only to view initial data structure</h5>
      <JSONTree data={tree}
                labelRenderer={([key]) => <strong>{key}:</strong>}
                valueRenderer={(raw) => <em>{raw}</em>}
                theme={{base00: '#272822'}}
                shouldExpandNode={() => true}
                getItemString={getItemString}
      />
    </div>
  );
};

export default memo(DBTreeJsonView);