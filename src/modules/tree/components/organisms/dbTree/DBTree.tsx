import React, {useCallback} from "react";
import { Space, notification } from 'antd';
import TButton from "../../atoms/TButton";
import DBTreeNodeList from "./DBTreeNodeList";
import { fetchItem } from "../../../services/dbApiService";
import {useTree} from "../../../hooks/useTreeContext";
import {SerializedDBRecord} from "mocks/types/treeData.interface";

import '../../styles/DBTree.scss';

interface Props {
}

const DBTree: React.FC<Props> = (): React.ReactElement<Props> | null => {

  const {selectedKey, setCacheItem, cacheItems, db, setCacheItems} = useTree();

  const handleClick = useCallback(async () => {
    if(selectedKey && cacheItems.findIndex((item: SerializedDBRecord) => item.key === selectedKey) < 0) {
      const newCacheItem = await fetchItem(selectedKey);
      setCacheItem(newCacheItem);
      setCacheItems([...cacheItems, newCacheItem]);
    } else {
      //Show personalized error message
      notification.open({
        message: 'Something went wrong',
        description:
          'Select an item / Be sure that item selected non exist yet'
      });
    }
  }, [selectedKey, cacheItems, setCacheItem, setCacheItems]);

  return (
    <div className='db-tree-viewer'>
      <h2>DBTreeView</h2>
      <Space>
        <TButton callback={handleClick} label={'Fetch item'} />
      </Space>
      <div className="content-wrapper">
        <DBTreeNodeList
          nodes={db}
        />
      </div>
    </div>
  );
};

export default DBTree;