import React, {useCallback} from "react";
import '../../styles/CachedTree.scss';
import { Space } from 'antd';
import CacheTreeNodeList from "./CacheTreeNodeList";
import TButton from "../../atoms/TButton";
import {useTree} from "../../../hooks/useTreeContext";
import {saveChangesInDB} from "../../../services/cacheService";

interface Props {
}

const CachedTree: React.FC<Props> = (): React.ReactElement<Props> | null => {
  const {tree, cacheItems, db, setDB, setSelectedKey} = useTree();

  const handleClick = useCallback(() => {
    setDB(saveChangesInDB([...db], [...cacheItems]));
    setSelectedKey(null);
  }, [cacheItems, db, setDB, setSelectedKey]);

  return (
      <div className='cache-tree-viewer'>
        <h2>CachedTreeView</h2>
        <Space>
          <TButton callback={handleClick} label={'Save changes in DB'} />
        </Space>
        <div className="content-wrapper">
          <CacheTreeNodeList
            nodes={tree}
          />
        </div>
      </div>
  );
};

export default CachedTree;