import React, {useCallback, useState} from "react";
import { Tree } from "mocks/types/treeData.interface";
import TStyledButton from "../../molecules/TStyledButton";
import { Input } from 'antd';
import { CloseOutlined, EditOutlined } from '@ant-design/icons';
import {useTree} from "../../../hooks/useTreeContext";
import {changeTreeState, updateCacheItem, updateItemsInCache} from "../../../services/cacheService";

interface Props {
  nodes: Tree[];
}

const CacheTreeNodeList: React.FC<Props> = ({ nodes }: Props): React.ReactElement<Props> => {
  const {cacheItems, setCacheItems} = useTree();
  const [isEditable, setIsEditable] = useState<number | null>(null);

  const updateItemCancelState = useCallback(async (key: number, state: boolean, node?: Tree) => {
    let cache = [ ...cacheItems ];
    const newCache = changeTreeState(cache, key, state);
    setCacheItems(newCache);
    //Remove / Restore stored Cache
    await updateItemsInCache(newCache);
  }, [cacheItems, setCacheItems]);

  const showEditForm = useCallback((key: number) => {
    setIsEditable(key);
  }, []);

  const editItem = useCallback(async (e, key: number) => {
    let cache = [ ...cacheItems ];
    cache[cache.findIndex(item => item.key === key)] = {...cache[cache.findIndex(item => item.key === key)], value: e.target.value};
    setCacheItems(cache);
    setIsEditable(null);
    // Update item in stored Cache
    await updateCacheItem(key, e.target.value, "value");
  }, [cacheItems, setCacheItems]);

    //TODO: separate into several components
    const renderTree = useCallback((data: Tree[]) => {
      return (
        <ul>
          {data?.map((node) => (
            // TODO: li - must become a molecule component
            <li key={node.key}>
              <div className="cache-node-wrapper">
                {/* TODO: Input must become a separate component*/}
                {isEditable && isEditable === node.key && !node.canceled ?
                  <div className="input-wrapper">
                  <Input placeholder="Insert new value" onPressEnter={e => editItem(e, node.key)} />
                    <TStyledButton callback={() => setIsEditable(null)} label="Close" />
                  </div> : <span className={node.canceled ? "rm-node-value" : "node-value"}>{node.value}</span>}
                {
                  node.canceled ?
                    <TStyledButton customClass="styled-btn_padding-helper" size="small" callback={() => updateItemCancelState(node.key, false)} label="Restore" /> :
                    <div className="btn-wrapper">
                      <TStyledButton customClass="styled-btn_padding-helper" shape="circle" size="small" callback={() => showEditForm(node.key)} icon={<EditOutlined />} />
                      <TStyledButton customClass="styled-btn_padding-helper" shape="circle" size="small" callback={() => updateItemCancelState(node.key, true, node)} icon={<CloseOutlined />} />
                    </div>
                }
              </div>
              {renderTree(node.children)}
            </li>
          ))}
        </ul>
      );
    }, [isEditable, showEditForm, editItem, updateItemCancelState]);

  return (
    <div className="cache-tree">
      {renderTree(nodes)}
    </div>
  )
}

export default CacheTreeNodeList;