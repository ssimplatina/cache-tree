import React from "react";
import "./styles/LayoutCentered.scss";

interface Props {
  children: React.ReactNode;
}

export default function LayoutCentered({children}: Props): React.ReactElement<Props> | null {

  return (
    <div className="wrapperCentered">
      <div className="container">
        {children}
      </div>
    </div>
  );
};