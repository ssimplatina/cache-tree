import React from "react";
import "./styles/LayoutColumns.scss";

interface Props {
  children: React.ReactNode;
}

export default function LayoutColumns({children}: Props): React.ReactElement<Props> | null {

  return (
    <div className="columnsWrapper">
        {children}
    </div>
  );
};