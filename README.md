# CacheTree project

### Description

1. Tree view contains three components:
    * CachedTree
    * DBTree
    * DBTreeJsonView (render initial data structure, it does not update with DB)
2. To simulate Cache / DB used two instances of IndexedDB
3. !IMPORTANT to update value in cache, press enter on the keyboard

## Available Scripts

In the project directory, you can run:

### `yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `yarn test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `yarn build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!
